const express = require("express");
const router= express.Router();

const { createUser,getAllUsers,getUsersByTeamName,changePassword} = require("./../controller/userController");
const {createTeam, getAllTeams,getTeamByUser,generateDemoData} =require("./../controller/teamController");
const {createGrade,getGradesAveragesByTeamId,getGradesAverageByUser} =require("./../controller/gradeController");
const {updateDescription,getPhase} =require("./../controller/phaseController");
const {populateQueue,getFromQueue}=require("./../controller/gradingQueueController");
const {userLogin}=require("./../controller/tokenController");
const {deleteAll,getDeadlines,setDeadlines}=require("./../controller/utilityController");    
    
    
    router.post("/login",userLogin)
    
    router.post("/team/users",getUsersByTeamName);
    router.post("/team/generate",generateDemoData);
    router.post("/team/grades",getGradesAveragesByTeamId);
    router.post("/teams",getAllTeams);
    router.post("/team",createTeam);
    
    
    router.post("/get-deadlines",getDeadlines);
    router.post("/set-deadlines",setDeadlines);
    
    router.post("/user/grade",createGrade);
    router.post("/user/grades",getGradesAverageByUser)
    router.post("/user/phaseEdit",updateDescription);
    router.post("/user/phase",getPhase);
    router.post("/users/populateQueue",populateQueue);
    router.post("/user/gradeQueue",getFromQueue);
    router.post("/user/changePassword",changePassword);
    router.post("/users",getAllUsers);
    router.post("/user",createUser);
    
    router.delete("/reset",deleteAll);
    
    module.exports = router;