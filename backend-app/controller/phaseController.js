const phaseService = require("./../service/phaseService");
const tokenService = require("./../service/tokenService");

//nu se mai foloseste, livrabilele se fac automat cand este facut un user
const createPhase = async (req, res, next) => {
    const phase = req.body;
    if(phase.phase_number&&phase.userId) {
        try{
        const result = await phaseService.create(phase);
        res.status(201).send({
            message: 'Phase added successfully.'
        });
        }
        catch(err)
        {
            console.log(err.message)
            res.status(500).send({
                 message: "Internal server error"
            })
        }
    } else {
        res.status(400).send({
            message: 'Invalid phase payload'
        });
    }
}

const updateDescription = async(req,res,next)=>{
    const data=req.body;
     if(req.body.secret){
        const tokenUser=await tokenService.getUserByToken(req.body.secret);
        if(tokenUser){
            if(data.userId&&data.phase_number){
                if((data.userId==tokenUser.id)||tokenUser.role==1){
                    try{
                    const result=await phaseService.updateDesc(data);
                    if(result==0)
                        res.status(200).send({message: "Description updated successfully"});
                    else
                        if(result==-1)
                            res.status(400).send({message: "Phase not found"})
                        else if(result==1)
                                res.status(403).send({message: "Cannot change past deadline"})
                }
                    catch(err)
                    {
                        console.log(err.message);
                         res.status(500).send({
                        message: 'Internal server error'
                    });
                    }
                }
                else
                {
                    res.status(401).send({message: "Unauthorised"});
                }
            }
            else{
                res.status(400).send({
                    message: 'Invalid payload'
                });
            }
        }
        else{
            res.status(401).send({message: "Unauthorised"});
        }
    }
    else{
        res.status(401).send({message: "Unauthorised"});
    }
}

const getPhase = async(req,res,next)=>{
    const data=req.body;
    if(req.body.secret){
        const tokenUser=await tokenService.getUserByToken(req.body.secret)
        if(tokenUser){
            if(data.userId&&data.phase_number){
                if(tokenUser.role==1||data.userId==tokenUser.id){
                const phase=await phaseService.getPhase(data);
                res.status(200).send(phase);
                }
                else{
                    res.status(401).send({message: "Unauthorised"});
                }
            }
            else
                res.status(400).send({
                    message: 'Invalid payload'
                });
            }
            else{
                res.status(401).send({message: "Unauthorised"});
            }
    }
    else
    {
        res.status(401).send({message: "Unauthorised"});
    }
}



module.exports = {
    createPhase,
    updateDescription,
    getPhase
}