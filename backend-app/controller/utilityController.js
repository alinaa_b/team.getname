const teamService = require("./../service/teamService");
const gradeService = require("./../service/gradeService");
const userService = require("./../service/userService");
const phaseService = require("./../service/phaseService");
const gradingQueueService=require("./../service/gradingQueueService");
const tokenService = require("./../service/tokenService");
const configuration  = require('./../config/configuration.json');
const fs=require('fs');

const deleteAll = async(req,res,next)=>{
    try{
        if(req.body.secret){
            const tokenUser=await tokenService.getUserByToken(req.body.secret);
            if(tokenUser){
                if(tokenUser.role!=1){
                    res.status(401).send({message: "Unauthorised"});
                }
                else
                {
                    await tokenService.destroyAll();
                    await gradingQueueService.destroyAll();
                    await phaseService.destroyAll();
                    await gradeService.destroyAll();
                    await userService.destroyAll();
                    await teamService.destroyAll();
                    configuration.backend.setDeadlines=[];
                    var data=JSON.stringify(configuration,null,2);
                    fs.writeFileSync('config/configuration.json',data,(err)=>{
                                if(err)
                                    throw new Error(err.message);
                            });
                    res.status(200).send({
                        message: "All data destroyed."
                    });
                }
            }
            else
            {
                res.status(401).send({message: "Unauthorised"});
            }
        }
        else
        {
            res.status(401).send({message: "Unauthorised"});
        }
    }
 catch(err) {
     console.warn(err.message);
        res.status(500).send({
            message: 'Internal server error'
        });
    }
    
}

const setDeadlines = async(req,res,next)=>{
    try{
        if(req.body.secret){
            const tokenUser=await tokenService.getUserByToken(req.body.secret);
            if(tokenUser){
                if(tokenUser.role!=1){
                    res.status(401).send({message: "Unauthorised"});
                }
                else
                {
                    if(req.body.phase_number&&req.body.deadline){
                        var dateCheck=new Date(req.body.deadline);
                        if((dateCheck instanceof Date) && (isFinite(dateCheck))
                        &&([0,1,2].includes(req.body.phase_number))){
                            configuration.backend.deadline[req.body.phase_number]=req.body.deadline;
                            var data=JSON.stringify(configuration,null,2);
                            fs.writeFileSync('config/configuration.json',data,(err)=>{
                                if(err)
                                    throw new Error(err.message);
                            });
                            
                            res.status(200).send({
                                message: "Deadlines set"
                            });
                        }
                        else
                        {
                            res.status(400).send({message: "Invalid payload"});
                        }
                        
                    }
                    else
                    {
                        res.status(400).send({message: "Invalid payload"});
                    }
                }
            }
            else{
                res.status(401).send({message: "Unauthorised"});
            }
        }
        else{
            res.status(401).send({message: "Unauthorised"});
        }
    }
    catch(err) {
     console.warn(err.message);
        res.status(500).send({
            message: 'Internal server error'
        });
    }
}

const getDeadlines = async(req,res,next)=>{
    try{
        if(req.body.secret){
            const tokenUser=await tokenService.getUserByToken(req.body.secret);
            if(tokenUser){
                res.status(200).send(configuration.backend.deadline);
            }
            else
            {
                res.status(401).send({message: "Unauthorised"});
            }
        }
        else{
            res.status(401).send({message: "Unauthorised"});
        }
    }
    catch(err) {
     console.warn(err.message);
        res.status(500).send({
            message: 'Internal server error'
        });
    }
}


module.exports = {
    deleteAll,
    getDeadlines,
    setDeadlines
}