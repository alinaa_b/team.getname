const gradeService = require("./../service/gradeService");
const userService = require("./../service/userService");
const phaseService = require("./../service/phaseService");
const gradingQueueService=require("./../service/gradingQueueService");
const tokenService = require("./../service/tokenService");

const createGrade = async (req, res, next) => {
    const grade = req.body;
    if(req.body.secret){
        const tokenUser=await tokenService.getUserByToken(req.body.secret);
        if(tokenUser){
            if(grade.grade&&grade.phaseId) {
                try{
                    const phase = await phaseService.getPhaseById(grade.phaseId);
                    if(await gradingQueueService.validate(tokenUser.id,grade.phaseId)){
                        await gradeService.create({userId: phase.userId,grade:grade.grade,phase_number:phase.phase_number});
                        await gradingQueueService.removeFromQueue({userId:tokenUser.id,phaseId:grade.phaseId});
                        res.status(201).send({
                            message: 'Grade added successfully.'
                        });
                    }
                    else
                    {
                        res.status(401).send({message: "Unauthorised"});
                    }
                }
                catch(err){
                    console.log(err.message)
                    res.status(500).send({
                        message: 'Internal server error'
                    });
                }
            } else {
                res.status(400).send({
                    message: 'Invalid grade payload'
                });
            }
        }
        else{
            res.status(401).send({message: "Unauthorised"});
        }
    }
    else{
        res.status(401).send({message: "Unauthorised"});
    }
}

const getGradesAverageByUser = async(req,res,next)=>{
    const grade = req.body;
    if(req.body.secret){
        const tokenUser=await tokenService.getUserByToken(req.body.secret);
        if(tokenUser){
            if(grade.phase_number&&grade.userId){
                try {
                    if((tokenUser.id==grade.userId)||(tokenUser.role==1)){
                        
                        const avg=getGradesAvgByUserInternal(grade.userId,grade.phase_number);
                        if(avg==-1){
                            res.status(200).send({
                                message:'Phase not completely graded yet.'
                            })
                        }
                        else
                        {
                            res.status(200).send(avg);
                        }
                    }
                    else
                    {
                        res.status(401).send({message: "Unauthorised"});
                    }
                    
                } catch(err) {
                    res.status(500).send({
                        message: 'Internal server error'
                    });
                }
            }
            else
            {
                res.status(400).send({
                    message: 'Invalid grade payload'
                });
            }
        }
        else{
            res.status(401).send({message: "Unauthorised"});
        }
    }
    else
    {
        res.status(401).send({message: "Unauthorised"});
    }
}

const getGradesAvgByUserInternal = async(userId,phase_number)=>{
    const grades = await gradeService.getByUser(userId,phase_number);
    const phase= await phaseService.getPhase({userId:userId,phase_number:phase_number});
        if(gradingQueueService.validatebyPhase(phase.id)){
            
            var max=Math.max(...grades);
            var min=Math.min(...grades);
            var avg=(grades.reduce((a,b)=>a+b,0)-max-min)/(grades.length-2);
            
            return avg;
        }
        else
        {
           return -1;
        }
}

const getGradesAveragesByTeamId = async (req, res, next) => {
    const data = req.body;
    try {
        if(req.body.secret){
            const tokenUser=await tokenService.getUserByToken(req.body.secret);
            if(tokenUser){
                if(data.teamId&&data.phase_number) {
                    if((tokenUser.teamId==data.teamId)||(tokenUser.role==1)){
                        const users = await userService.getByTeamId(data.teamId);
                        let result=[];
                        var grades;
                        for(var i=0;i<users.length;i++){
                            if(users[i].role==0){
                            grades = await getGradesAvgByUserInternal(users[i].id,data.phase_number)
                            result.push(grades);
                            }
                        }
                        
                        res.status(200).send(result);
                    }
                } else {
                    res.status(400).send({
                        message: 'No team specified'
                    })
                }
            }
            else{
                res.status(401).send({message: "Unauthorised"});
            }
        }
        else{
            res.status(401).send({message: "Unauthorised"});
        }
    } catch(err) {
        console.log(err.message)
        res.status(500).send({
            message: 'Internal server error'
        })
    }
}

module.exports = {
    createGrade,
    getGradesAveragesByTeamId,
    getGradesAverageByUser
    
}