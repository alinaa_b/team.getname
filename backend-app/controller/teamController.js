const teamService = require("./../service/teamService");
const gradeService = require("./../service/gradeService");
const userService = require("./../service/userService");
const phaseService = require("./../service/phaseService");
const gradingQueueService=require("./../service/gradingQueueService");
const tokenService = require("./../service/tokenService");

const createTeam = async (req, res, next) => {
    if(req.body.secret){
        const tokenUser=await tokenService.getUserByToken(req.body.secret);
        if(tokenUser){
            if(tokenUser.role!=1){
                res.status(401).send({message: "Unauthorised"});
            }
            else{
                const team = req.body;
                if(team.team_name) {
                    try{
                        const existingTeam= await teamService.getByTeamName(team.team_name);
                        if(!existingTeam){
                        var result = await teamService.create(team);
                        var user1;
                        var user2;
                        var user3;
                        var user4;
                        var user5;
                        if(team.user1){
                            team.user1.teamId=result.id;
                            user1=await userService.create(team.user1);
                            await phaseService.create({"phase_number":1,"userId":user1.dataValues.id})
                            await phaseService.create({"phase_number":2,"userId":user1.dataValues.id})
                            await phaseService.create({"phase_number":3,"userId":user1.dataValues.id})
                        }
                        if(team.user2){
                            team.user2.teamId=result.id;
                            user2=await userService.create(team.user2);
                            await phaseService.create({"phase_number":1,"userId":user2.dataValues.id})
                            await phaseService.create({"phase_number":2,"userId":user2.dataValues.id})
                            await phaseService.create({"phase_number":3,"userId":user2.dataValues.id})
                        }
                        if(team.user3){
                            team.user3.teamId=result.id;
                            user3=await userService.create(team.user3);
                            await phaseService.create({"phase_number":1,"userId":user3.dataValues.id})
                            await phaseService.create({"phase_number":2,"userId":user3.dataValues.id})
                            await phaseService.create({"phase_number":3,"userId":user3.dataValues.id})
                        }
                        if(team.user4){
                            team.user4.teamId=result.id;
                            user4=await userService.create(team.user4);
                            await phaseService.create({"phase_number":1,"userId":user4.dataValues.id})
                            await phaseService.create({"phase_number":2,"userId":user4.dataValues.id})
                            await phaseService.create({"phase_number":3,"userId":user4.dataValues.id})
                        }
                        if(team.user5){
                            team.user5.teamId=result.id;
                            user5=await userService.create(team.user5);
                            await phaseService.create({"phase_number":1,"userId":user5.dataValues.id})
                            await phaseService.create({"phase_number":2,"userId":user5.dataValues.id})
                            await phaseService.create({"phase_number":3,"userId":user5.dataValues.id})
                        }
                        res.status(201).send({
                            message: 'Team added successfully.'
                        });
                        }
                        else
                        {
                             res.status(403).send({
                            message: 'Team already exists.'
                            });
                        }
                    }
                    catch(err)
                    {
                        console.warn(err.message)
                        if(user1){
                            user1.destroy();
                        }
                        if(user2){
                            user2.destroy();
                        }
                        if(user3){
                            user3.destroy();
                        }
                        if(user4){
                            user4.destroy();
                        }
                        if(user5){
                            user5.destroy();
                        }
                        if(result){
                            result.destroy();
                        }
                        res.status(500).send({
                            message: 'Internal server error'
                        });
                    }
                } else {
                    res.status(400).send({
                        message: 'Invalid team payload.'
                    });
                }
            }
        }
        else{
            res.status(401).send({message: "Unauthorised"});
        }
    }
    else{
        res.status(401).send({message: "Unauthorised"});
    }
}

const getAllTeams = async (req, res, next) => {
    try {
        if(req.body.secret){
            const tokenUser=await tokenService.getUserByToken(req.body.secret);
            if(tokenUser){
                if(tokenUser.role!=1){
                    res.status(401).send({message: "Unauthorised"});
                }
                else
                {
                    const teams = await teamService.getAll();
                    res.status(200).send(teams);
                }
            }
            else{
            res.status(401).send({message: "Unauthorised"});
            }
        }
        else
        {
            res.status(401).send({message: "Unauthorised"});
        }
    } catch(err) {
        res.status(500).send({
            message: 'Internal server error'
        });
    }
}

const getTeamByUser = async (req, res, next) => {
    try {
        const category = req.body.category;
        if(req.body.secret){
            const tokenUser=await tokenService.getUserByToken(req.body.secret);
            if(tokenUser){
                if(tokenUser.role!=1){
                    res.status(401).send({message: "Unauthorised"});
                }
                else
                {
                        if(req.body.user) {
                        try {
                            const team = await teamService.getTeamByID(tokenUser.teamId);
                            res.status(200).send(team);
                        } catch(err) {
                            res.status(500).send({
                                message: 'Internal server error'
                            })
                        }
                    } else {
                        res.status(400).send({
                            message: 'User not found'
                        })
                    }
                }
            }
            else{
                res.status(401).send({message: "Unauthorised"});
            }
        }
        else{
            res.status(401).send({message: "Unauthorised"});
        }
    } catch(err) {
        res.status(500).send({
           message: 'Internal server error'
        })
    }
}

const generateDemoData = async(req,res,next)=>{
    try{ 
        if(req.body.secret){
            const tokenUser=await tokenService.getUserByToken(req.body.secret);
            if(tokenUser){
                if(tokenUser.role!=1){
                    res.status(401).send({message: "Unauthorised"});
                }
                else
                {
                    for(var i=0;i<20;i++){
                        const team=await teamService.create({"team_name":'Team'+i})
                        for(var j=0;j<5;j++){
                            const user= await userService.create({
                                "username":"User"+i+j,
            	            "fullname":"gica"+i+j,
            	            "email":"nu",
            	            "password":"da",
            	            "role":0,
            	            "teamId":team.dataValues.id});
            	            await phaseService.create({"phase_number":1,"userId":user.dataValues.id})
                            await phaseService.create({"phase_number":2,"userId":user.dataValues.id})
                            await phaseService.create({"phase_number":3,"userId":user.dataValues.id})
                        }
                    }
                    res.status(200).send({
                        message: "Data generated"
                    })
                }
            }
            else{
            res.status(401).send({message: "Unauthorised"});
            }
        }
    }
    catch(err)
    {
        console.log(err.message)
        res.status(500).send({
            message: 'Internal server error'
        })
    }
}


module.exports = {
    createTeam,
    getAllTeams,
    getTeamByUser,
    generateDemoData,
}