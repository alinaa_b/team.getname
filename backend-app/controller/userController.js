const userService = require("./../service/userService");
const teamService=require("./../service/teamService");
const phaseService=require("./../service/phaseService");
const tokenService = require("./../service/tokenService");
const gradeService = require("./../service/gradeService");
const gradingQueueService=require("./../service/gradingQueueService");

const createUser = async (req, res, next) => {
    const user = req.body;
    if(req.body.secret){
        const tokenUser=await tokenService.getUserByToken(req.body.secret);
        if(tokenUser){
            if(tokenUser.role!=1){
                res.status(401).send({message: "Unauthorised"});
            }
            else{
                if(user.username && user.fullname && user.email && user.password &&user.role) {
                    try{
                        const existingUser= await userService.getByUsername(user.username);
                        if(!existingUser)
                        {
                            var result=await userService.create(user);
                            console.log(result);
                            
                            await phaseService.create({"phase_number":1,"userId":result.dataValues.id})
                            await phaseService.create({"phase_number":2,"userId":result.dataValues.id})
                            await phaseService.create({"phase_number":3,"userId":result.dataValues.id})
                            
                            res.status(201).send({
                                message: 'User added successfully.'
                            })
                        }
                        else
                        {
                            res.status(403).send({
                            message: 'User already exists.'})
                        }
                    }
                    catch(err){
                            console.log(err.message)
                        res.status(500).send({
                            message: 'Internal server error'
                        })
                    }
                } else {
                    res.status(400).send({
                        message: 'Invalid user payload.'
                    });
                }
            }
        }
        else{
            res.status(401).send({message: "Unauthorised"});
        }
    }
    else
    {
        res.status(401).send({message: "Unauthorised"});
    }
}

const adminInit =async()=>{
    try{
        const admin=await userService.getByRole(1);
        if(!admin){
            userService.create({username:"Admin",password:"Admin",fullname:"Admin",email:"nu",role:1})
        }
    }
    catch(err) {
     console.warn(err.message);
        
    }
}

const getAllUsers = async (req, res, next) => { //TODO: remove password from returned data
    try {
        if(req.body.secret){
            const tokenUser=await tokenService.getUserByToken(req.body.secret);
            if(tokenUser){
                if(tokenUser.role!=1){
                    res.status(401).send({message: "Unauthorised"});
                }
                else{
                    const users = await userService.getAll();
                    for(var i=0;i<users.length;i++){
                            users[i].password="";
                        }
                    res.status(200).send(users);
                }
            }
            else{
                res.status(401).send({message: "Unauthorised"});
            }
        }
        else
        {
            res.status(401).send({message: "Unauthorised"});
        }
    } catch(err) {
        res.status(500).send({
            message: 'Internal server error'
        });
    }
}

const banUser=async (req, res, next)=>{
    try{
        if(req.body.secret){
        const tokenUser=await tokenService.getUserByToken(req.body.secret);
            if(tokenUser){
                if(tokenUser.role!=1){
                    res.status(401).send({message: "Unauthorised"});
                }
                else{
                    if(req.body.username){
                        const toBan=await userService.getByUsername(req.body.username);
                        await tokenService.destroyUserTokens(toBan.id);
                        await gradeService.destroyUserGrades(toBan.id);
                        await phaseService.destroyUserPhases(toBan.id);
                        await gradingQueueService.destroyUserQueue(toBan.id);
                        await userService.destroyUser(toBan.id);
                    }
                    else{
                         res.status(400).send({
                        message: 'Invalid user payload.'
                    });
                    }
                }
            }
            else{
                res.status(401).send({message: "Unauthorised"});
            }
        }
        else
        {
            res.status(401).send({message: "Unauthorised"});
        }
    }
    catch(err) {
        res.status(500).send({
            message: 'Internal server error'
        });
    }
}

const getUsersByTeamName = async (req, res, next) => {
    try {
        if(req.body.secret){
            const tokenUser=await tokenService.getUserByToken(req.body.secret);
            if(tokenUser){
                if(req.body.team_name) {
                    const team= await teamService.getByTeamName(req.body.team_name);
                    if((tokenUser.teamId!=team.id)&&tokenUser.role!=1)
                        res.status(401).send({message: "Unauthorised"});
                    else{
                        const users = await userService.getByTeamId(team.id);
                        for(var i=0;i<users.length;i++){
                            users[i].password="";
                        }
                        res.status(200).send(users);
                    }
                }
                else {
                    res.status(400).send({
                        message: 'No team specified'
                    })
                }
            }
            else{
                res.status(401).send({message: "Unauthorised"});
            }
        }
        else
        {
            res.status(401).send({message: "Unauthorised"});
        }
        
    } catch(err) {
        res.status(500).send({
            message: 'Internal server error'
        })
    }
}

const changePassword = async (req,res,next)=>{
    try{
        const userData=req.body;
        if(userData.secret){
        const tokenUser=await tokenService.getUserByToken(userData.secret);
        if(tokenUser){
            if(tokenUser.username!=userData.username)
                res.status(401).send({message: "Unauthorised"});
            else{
                if(userData.username&&userData.oldPass&&userData.newPass){
                    if(tokenUser.password!=userData.oldPass){
                        res.status(401).send({message: "Unauthorised"});
                    }
                    else{
                        const result= await userService.changePassword(userData);
                        if(result==0) {
                            res.status(200).send({message: "Password changed successfully"});
                        }
                        else{
                            res.status(500).send({message: "Internal server error"})
                        }
                    }
                }
                else
                {
                    res.status(400).send({
                        message: 'Invalid payload'
                    })
                }
            }
        }
    }
    else
    {
        res.status(401).send({
                message: 'Unauthorised'
            })
    }
    
}
    catch(err) {
        res.status(500).send({
            message: 'Internal server error'
        })
    }
}

module.exports = {
    createUser,
    getAllUsers,
    getUsersByTeamName,
    changePassword,
    adminInit
}