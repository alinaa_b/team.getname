const gradingQueueService=require("./../service/gradingQueueService");
const userService = require("./../service/userService");
const phaseService = require("./../service/phaseService");
const tokenService = require("./../service/tokenService");
const configuration  = require('./../config/configuration.json');
const fs=require('fs');


const populateQueue= async(req,res,next)=>{
    const data=req.body;
    try{
        if(req.body.secret){
            const tokenUser=await tokenService.getUserByToken(req.body.secret);
            if(tokenUser){
                if(tokenUser.role==1){
                    if(data.phase_number)
                    {
                       populateQueueInternal(data.phase_number)
                        res.status(200).send({
                            message: "Queue populated"
                        })
                    }
                    else
                    {
                        res.status(400).send({
                        message: 'Invalid payload'
                    });
                    }
                }
                else
                {
                    res.status(401).send({message: "Unauthorised"});
                }
            }
            else{
                res.status(401).send({message: "Unauthorised"});
            }
        }
        
        else{
            res.status(401).send({message: "Unauthorised"});
        }
    }
    catch(err)
    {
        console.log(err.message)
        res.status(500).send({
             message: "Internal server error"
        })
    }
}

const automaticQueuePopulation = async()=>{
    const now=new Date();
    const deadline1=new Date(configuration.backend.deadline[0]);
    const deadline2=new Date(configuration.backend.deadline[1]);
    const deadline3=new Date(configuration.backend.deadline[2]);
    if((now.getYear()==deadline1.getYear())
    &&(now.getMonth()==deadline1.getMonth())
    &&(now.getDay()==deadline1.getDay()))
    {
        if(!configuration.backend.setDeadlines.includes(1)){
            populateQueueInternal(1);
            configuration.backend.setDeadlines.push(1);
            var data=JSON.stringify(configuration,null,2);
                        fs.writeFileSync('config/configuration.json',data,(err)=>{
                            if(err)
                                throw new Error(err.message);
                        });
        }
        
        setTimeout(automaticQueuePopulation, 86400000); // va rula o data pe zi
    }
    else{
        if((now.getYear()==deadline2.getYear())
        &&(now.getMonth()==deadline2.getMonth())
        &&(now.getDay()==deadline2.getDay()))
        {
            if(!configuration.backend.setDeadlines.includes(2)){
                populateQueueInternal(2);
                configuration.backend.setDeadlines.push(2);
                var data=JSON.stringify(configuration,null,2);
                            fs.writeFileSync('config/configuration.json',data,(err)=>{
                                if(err)
                                    throw new Error(err.message);
                            });
            }
            
            setTimeout(automaticQueuePopulation, 86400000);
        }
        else{
            if((now.getYear()==deadline3.getYear())
            &&(now.getMonth()==deadline3.getMonth())
            &&(now.getDay()==deadline3.getDay()))
            {
                if(!configuration.backend.setDeadlines.includes(3)){
                    populateQueueInternal(3);
                    configuration.backend.setDeadlines.push(3);
                    var data=JSON.stringify(configuration,null,2);
                                fs.writeFileSync('config/configuration.json',data,(err)=>{
                                    if(err)
                                        throw new Error(err.message);
                                });
                }
            }
            else
            {
                setTimeout(automaticQueuePopulation, 86400000);
            }
            
        }
    }
}

const populateQueueInternal = async(phase_number)=>{
    
    const users= await userService.getAll();
    const offset=(Math.floor(Math.random()*(users.length-50)))
    for(var i=0;i<users.length;i++){
        if(users[i].role==0){
            var userToGrade=i+offset;
            for(var j=1;j<=10;j++){
                userToGrade+=(j*5);
                if(userToGrade>users.length)
                    userToGrade-=users.length;
                    
                if(users[userToGrade].role==0){
                    const phase=await phaseService.getPhase({userId:users[userToGrade].id,phase_number:phase_number})
                    gradingQueueService.create({userId:users[i].id,phaseId:phase.id})
                }
            }   
        }
    }
}

const getFromQueue =async(req,res,next)=>{
    try{
        if(req.body.secret){
            const tokenUser=await tokenService.getUserByToken(req.body.secret);
            if(tokenUser){
                const fromQueue=await gradingQueueService.getFromQueue(tokenUser.id);
                if(fromQueue){
                    const phase=await phaseService.getPhaseById(fromQueue.phaseId);
                    var result=phase;
                    result.userId=null;
                    res.status(200).send(result)
                }
                else{
                    res.status(200).send({
                        message: "Queue empty"
                    })
                }
            }
            else{
                res.status(401).send({message: "Unauthorised"});
            }
        }
        else
        {
            res.status(401).send({message: "Unauthorised"});
        }
    }
    catch(err)
    {
        console.log(err.message)
        res.status(500).send({
             message: "Internal server error"
        })
    }
}

module.exports = {
    populateQueue,
    getFromQueue,
    automaticQueuePopulation
}