const tokenService = require("./../service/tokenService");
const userService = require("./../service/userService");


const userLogin = async (req,res,next)=>{
    try{
        const userData=req.body;
        const result=await userService.userLogin(userData);
        if(result){
            const secret=await tokenService.createToken(result.id);
            res.status(200).send({
                message: 'Welcome '+userData.username,
                secret:secret
            })
        }
        else
        {
            res.status(403).send({
                status:403,
                message: "Invalid credentials!"})
        }
    }
    catch(err) {
        console.log(err.message)
        res.status(500).send({
            message: 'Internal server error'
        })
    }
}

module.exports={userLogin};