const {GradingQueue} = require("./../models/database");


const gradingQueue={
    create: async(data)=>{
        try{
            await GradingQueue.create(data);
        }
        catch(err)
        {
            throw new Error(err.message);
        }
    },
    removeFromQueue: async(data)=>{
        try{
            await GradingQueue.destroy({where: {userId:data.userId,phaseId:data.phaseId}});
        }
        catch(err)
        {
            throw new Error(err.message);
        }
    },
    getFromQueue: async(userId)=>{
        try{
            var result= await GradingQueue.findOne({where: {userId:userId}})
            return result;
        }
        catch(err)
        {
            throw new Error(err.message);
        }
    },
    validate: async(userId,phaseId)=>{
        try{
            const result=await GradingQueue.findOne({where: {userId:userId,phaseId:phaseId}});
            if(result)
                return true;
            else
                return false;
        }
        catch(err)
        {
            throw new Error(err.message);
        }
    },
    validatebyPhase: async(phaseId)=>{
      try{
            const result=await GradingQueue.findOne({where: {phaseId:phaseId}});
            if(result)
                return true;
            else
                return false;
        }
        catch(err)
        {
            throw new Error(err.message);
        }  
    },
    destroyAll: async()=>{
        try{
            await GradingQueue.destroy({where:{}});
        }
        catch(err)
        {
            throw new Error(err.message)
        }
    },
     destroyUserQueue: async(userId)=>{
        try{
            await GradingQueue.destroy({where:{userId:userId}});
        }
        catch(err)
        {
            throw new Error(err.message)
        }
    }
    
    
};

module.exports = gradingQueue;