const { User,Token }=require('./../models/database.js');
const uuidv4=require('uuid/v4');

const token={
    createToken: async(userId)=>{
        try{
            var uuid=uuidv4();
            var notUnique;
            while(notUnique){
                if(Token.findOne({where: {secret: uuid}}))
                {
                    uuid=uuidv4();
                }
                else
                    notUnique=false;
            }
            var toCreate={
                userId:userId,
                secret:uuid
            }
            await Token.destroy({where: {userId:userId}});
            await Token.create(toCreate);
            return uuid;
        }
        catch(err){
            throw new Error(err.message);
        }
    },
    getUserByToken: async(uuid)=>{
        try{
            const token= await Token.findOne({where: {secret: uuid}});
            if(token){
                const user= await User.findOne({where: {id:token.userId}});
                return user;
            }
            else
                return null;
        }
        catch(err)
        {
            throw new Error(err.message)
        }
    },
    destroyAll: async()=>{
        try{
            await Token.destroy({where:{}});
        }
        catch(err)
        {
            throw new Error(err.message)
        }
    },
     destroyUserTokens: async(userId)=>{
        try{
            await Token.destroy({where:{userId:userId}});
        }
        catch(err)
        {
            throw new Error(err.message)
        }
    },
}

module.exports=token;