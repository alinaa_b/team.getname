const {Grade} = require('./../models/database.js');

const grade={
    create: async(grade) => {
        try{
            const result=await Grade.create(grade);
            return result;
        }
        catch(err)
        {
            throw new Error(err.message);
        }
    },
    getByUser: async(userIdToCheck,phase_number) =>{
        try{
            const result=await Grade.findAll({where: {userId: userIdToCheck,phase_number:phase_number}});
            return result;
        }
        catch(err)
        {
            throw new Error(err.message);
        }
    },
    destroyAll: async()=>{
        try{
            await Grade.destroy({where:{}});
        }
        catch(err)
        {
            throw new Error(err.message)
        }
    },
    destroyUserGrades: async(userId)=>{
        try{
            await Grade.destroy({where:{userId:userId}});
        }
        catch(err)
        {
            throw new Error(err.message)
        }
    }
}

module.exports=grade;