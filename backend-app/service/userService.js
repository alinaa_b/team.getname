const { User } = require("./../models/database");

const user = {
    create: async (user) =>{
        try{
            const result= await User.create(user);
            return result;
        }
        catch(err)
        {
            throw new Error(err.message);
        }
    },
    getAll: async () => {
        try{
            const users=await User.findAll();
            return users;
        }
        catch(err)
        {
            throw new Error(err.message);
        }
    },
    getByUsername: async (usernameToCheck) =>{
        try{
            const result=await User.findOne({where: {username: usernameToCheck}});
            return result;
        }
        catch(err)
        {
            throw new Error(err.message);
        }
    },
    getByRole: async (roleToCheck) =>{
        try{
            const result=await User.findOne({where: {role: roleToCheck}});
            return result;
        }
        catch(err)
        {
            throw new Error(err.message);
        }
    },
    getByEmail: async (EmailToCheck) =>{
        try{
            const result=User.findOne({where: {email: EmailToCheck}});
            return result;
        }
        catch(err)
        {
            throw new Error(err.message);
        }
    },
    getByTeamId: async (teamId)=>
    {
        try{
            const result=User.findAll({where: {teamId: teamId}});
            return result;
        }
        catch(err)
        {
            throw new Error(err.message);
        }
    },
    userLogin: async (loginData)=>{
        try{
            const result=User.findOne({where: {username: loginData.username, password: loginData.password}});
            return result;
        }
        catch(err)
        {
            throw new Error(err.message);
        }
    },
    changePassword: async (data)=>{
        try{
            const result= await User.findOne({where: {username: data.username}});
            if(result)
            {
                result.update({password: data.newPass})
                return 0;
            }
            else
                return -1;
        }
        catch(err)
        {
            throw new Error(err.message);
        }
    },
    destroyAll: async()=>{
        try{
            await User.destroy({where:{}});
        }
        catch(err)
        {
            throw new Error(err.message)
        }
    },
    destroyUser: async(userId)=>{
        try{
            await User.destroy({where:{id:userId}});
        }
        catch(err)
        {
            throw new Error(err.message)
        }
    }
}


module.exports = user;