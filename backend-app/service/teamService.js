const {Team} = require('./../models/database.js');

const team={
    create: async(team) => {
        try{
            const result=await Team.create(team);
            return result;
        }
        catch(err)
        {
            throw new Error(err.message);
        }
    },
    getByTeamName: async (nameToCheck) =>{
        try{
            const result=await Team.findOne({where: {team_name: nameToCheck}});
            return result;
        }
        catch(err)
        {
            throw new Error(err.message);
        }
    },
     getAll: async () => {
        try{
            const result=await Team.findAll();
            return result;
        }
        catch(err)
        {
            throw new Error(err.message);
        }
    },
    destroyAll: async()=>{
        try{
            await Team.destroy({where:{}});
        }
        catch(err)
        {
            throw new Error(err.message)
        }
    }
}

module.exports=team;