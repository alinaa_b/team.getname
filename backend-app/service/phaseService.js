const { Phase } = require("./../models/database");
const configuration  = require('./../config/configuration.json');

const phase = {
    create: async (phase) =>{
        try{
            const result= await Phase.create(phase);
            return result;
        }
        catch(err)
        {
            throw new Error(err.message);
        }
    },
    updateDesc: async(data)=>{
        try{
            const phase=await Phase.findOne({where: {userId: data.userId,phase_number: data.phase_number}});
            var today=new Date();
            var deadline=new Date(configuration.backend.deadline[data.phase_number])
            if(phase)
            {
                console.log(today)
                console.log(deadline)
                if(today>deadline){
                    return 1;    
                }
                else{
                    if(data.phase_description){
                        phase.update({phase_description: data.phase_description,phase_link:data.phase_link})
                    }
                    else
                    {
                        phase.update({phase_description: ""})
                    }
                    return 0;
                }
            }
            else
            {
            return -1;
            }
        }
        catch(err)
        {
            throw new Error(err.message);
        }
    },
    getPhase: async(data)=>{
        try{
            const phase=await Phase.findOne({where: {userId: data.userId,phase_number:data.phase_number}});
            return phase;
        }
        catch(err)
        {
            throw new Error(err.message);
        }
    },
    getPhaseById: async(id)=>{
        try{
            const phase=await Phase.findOne({where: {id: id}});
            return phase;
        }
        catch(err)
        {
            throw new Error(err.message);
        }
    },
    destroyAll: async()=>{
        try{
            await Phase.destroy({where:{}});
        }
        catch(err)
        {
            throw new Error(err.message)
        }
    },
    destroyUserPhases: async(userId)=>{
        try{
            await Phase.destroy({where:{userId:userId}});
        }
        catch(err)
        {
            throw new Error(err.message)
        }
    }
}


module.exports = phase;