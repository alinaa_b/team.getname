const configuration  = require('./../config/configuration.json');
const Sequelize = require('sequelize');

const DB_NAME = configuration.database.database_name;
const DB_USER = configuration.database.username;
const DB_PASS = configuration.database.password;

const sequelize = new Sequelize(DB_NAME, DB_USER, DB_PASS, {
    dialect: 'mysql'
});

sequelize.authenticate().then(() => {
    console.log('Database connection success!');
}).catch(err => {
    console.log(`Database connection error: ${err}`);
});


class Team extends Sequelize.Model {};
class User extends Sequelize.Model {};
class Grade extends Sequelize.Model {};
class Phase extends Sequelize.Model {}; //livrabilele
class GradingQueue extends Sequelize.Model {};
class Token extends Sequelize.Model {};


Team.init({
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    team_name: {
        type: Sequelize.STRING,
        allowNull: false,
        unique:true
    },
    project_link: {
        type: Sequelize.STRING,
        defaultValue:"none"
    },
    phase1_average_ofgrades: {
        type: Sequelize.DOUBLE,
        defaultValue:0
    },
    
       phase2_average_ofgrades: {
        type: Sequelize.DOUBLE,
        defaultValue:0
      }, 
       phase3_average_ofgrades: {
        type: Sequelize.DOUBLE,
        defaultValue:0
      }, 
      
  final_grade: {
        type: Sequelize.DOUBLE,
        defaultValue:0
      }
          
}, {
    sequelize,
    modelName: 'teams'
});

User.init({
    id: {
       type: Sequelize.INTEGER,
       primaryKey:true,
       autoIncrement:true
    },
    username: {
     type: Sequelize.STRING,
        allowNull: false,
        unique:true
    },
    fullname: {
        type: Sequelize.STRING,
        allowNull: false
    },
    email: {
        
        type: Sequelize.STRING,
        allowNull: false
    },
    password:
    {
        type: Sequelize.STRING,
        allowNull: false
        
    },
    role:
    {
        type: Sequelize.INTEGER, //0=student, 1=profesor, -1=blocat/banat
        allowNull: false
    }
    
},{ 
    sequelize,
    modelName: 'users'
});


Grade.init({
    id:
    {  type:Sequelize.INTEGER,
       primaryKey:true,
       autoIncrement:true
    },
    
    grade:
    { type:Sequelize.DOUBLE
        
    },
    phase_number:
    {
        type: Sequelize.INTEGER
    }
},{
    sequelize,
    modelName: 'grades'
});


Phase.init({
    id:
    {
        type:Sequelize.INTEGER,
       primaryKey:true,
       autoIncrement:true
    },
    phase_description: //descrierea livrabilului partial
    {
        type:Sequelize.STRING,
        defaultValue:"none"
    },
    phase_number:
    {
        type:Sequelize.INTEGER
    },
    phase_link:
    {
        type:Sequelize.STRING
    }
},{
    sequelize,
    modelName: 'phases'
});

GradingQueue.init({
     id:
    {
        type:Sequelize.INTEGER,
       primaryKey:true,
       autoIncrement:true
    },
    userId: //utilizatorul care va acorda nota
    {
        type: Sequelize.INTEGER,
    },
    phaseId: //livrabilul care va fi notat
    {
        type: Sequelize.INTEGER,
    }
    
},{
    sequelize,
    modelName: 'gradingqueue'
});

Token.init({
    id:{
        type:Sequelize.INTEGER,
       primaryKey:true,
       autoIncrement:true
    },
    secret:{
        type:Sequelize.UUID,
        unique: true,
        allowNull:false
    }
    
},{
    sequelize,
    modelName: 'tokens'
})

Team.hasMany(User);
User.belongsTo(Team);
User.hasMany(Grade);
Grade.belongsTo(User);
User.hasMany(Phase);
Phase.belongsTo(User);
User.hasOne(Token);
Token.belongsTo(User);



Team.sync().then(()=>{
    User.sync()
})
.then(()=>{
    Phase.sync()
})
.then(()=>{
    Grade.sync()
    })
.then(()=>{
    GradingQueue.sync()
})
.then(()=>{
    Token.sync()
});




module.exports = {
    sequelize,
    Team,
    User,
    Grade,
    Phase,
    GradingQueue,
    Token
}
