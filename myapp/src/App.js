import React, { Component } from 'react';
import './App.css';
import { BrowserRouter as Router, Switch } from 'react-router-dom';
import CustomRoute from './components/custom-route/CustomRoute';

import Login from './pages/Login/Login'
import UserAdmin from './pages/UserAdmin/UserAdmin'

class App extends Component {

  render() {
    return (
      <div>
      <Router>
          <Switch>
            <CustomRoute path={"/login"}  Component={Login} />
            <CustomRoute path={"/userAdmin"}  Component={UserAdmin} />
          </Switch>
      </Router>
      </div>
    );
  }
}

export default App;
