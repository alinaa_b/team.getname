import React from 'react'
import { Link, withRouter } from "react-router-dom";
const SERVER =require('../../config');
// import 'isomorphic-fetch';
// import 'es6-promise';

//const url = "http://18.216.193.146:8080/api/login";
const url = 'http://'+SERVER.SERVER+":8080/api/login";

class Login extends React.Component{
    
    constructor(props){
        super(props);
        this.state={
            username: "",
            password: ""
        };
    }
    
    handleSubmit = e => {
        e.preventDefault();
        const data = {
            username: this.state.username,
            password: this.state.password
        };

    try {
      window.fetch(url, {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
          "Content-Type": "application/json"
        }
      }).then(result => result.json())
        .then(res => {
          if (res.secret) {
            window.localStorage.setItem("secret", res.secret);
            this.props.history.push("/userAdmin");
            // alert("Login Successfully");
          }else{
              alert("Check your credentials");
          }
        });
    } catch (error) {
      console.error("Error:", error);
    }
  };
    
    handleChangeUsername = (event) => {
      this.setState({
          username: event.target.value
      });
      if(document.getElementById("usernameInput").style["backgroundColor"] === "red")
      {
          document.getElementById("usernameInput").style["backgroundColor"] = "white";
      }
    };
    
    handleChangePassword = (event) => {
        this.setState({
            password: event.target.value
        });
        if(document.getElementById("passwordInput").style["backgroundColor"] === "red")
      {
          document.getElementById("passwordInput").style["backgroundColor"] = "white";
      }
    }
    
    
    render(){
        return(
            <div style={{border: '0.5rem outset #28455e','borderRadius': '12px',padding: '1rem','backgroundColor': '#acd2ff','alignItems': 'center'}}>
            <form onSubmit={this.handleSubmit}>

                <div style={{'paddingTop': '0px'}}>
                    <h1>Username/email</h1>
                    <input id="usernameInput" type="Text" placeholder="Username" value={this.state.username}
                        onChange={this.handleChangeUsername }
                    />
                </div>
                <div style={{'paddingBottom': '5px'}}>
                    <h1>Password</h1>
                    <input id="passwordInput" type="Password" placeholder="Password" value={this.state.password}
                        onChange={this.handleChangePassword }
                    />
                </div>
                <div style={{width: '50%','marginLeft': '25%'}}>
            
            
                <button type='submit'>Login</button>
                </div>
            </form>
        
        </div>
        );
    }
}

export default Login;
