import React, { Component } from "react";
import Table, { flatten, unflatten } from "tablex";
import axios from 'axios';
import { Link, withRouter } from 'react-router-dom';
const SERVER =require('../../config');


//const url = 'http://18.216.193.146:8080/api/teams';
const url = 'http://'+SERVER.SERVER+':8080/api/teams';


class UserAdmin extends React.Component {
  constructor(props) {
    super(props);
    this.state={
      teams:[],
    }
  }

  async componentDidMount() {
      const secret = window.localStorage.getItem("secret");
      let data = {"secret": secret};
      
      const response = await axios.post(url, data, {
        headers: { "Content-Type": "application/json" }
      });
      
    let receivedTeams = response.data;
      this.setState({
        teams: receivedTeams
      });
      
}
render(){
    return(
      
      <div> Aici trebuia sa fie un tabel...
      <ul>
            {this.state.teams.map(item => (
            <li key={item}>{item.team_name}  |  {item.project_link}   |  {item.phase1_average_ofgrades}   |  {item.phase2_average_ofgrades} | {item.phase3_average_ofgrades}  |   {item.final_grade}  |   {item.createdAt}</li>
          ))}
      </ul>
      </div>
    )
}
}

export default UserAdmin;